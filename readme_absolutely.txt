#this terrier project is intended for use with eclipse projects and jar packages in general, that is,
in this directory I usually add and modify the terrier code to meet my
necessities. I keep this code in a Bitbucket depository. I modified this version of terrier to meet my special requirements with new methods and implementations of weighting models.

#Other usable versions of terrier can be found in Documents/terrier, and those are
the ones to use if you want a standard Terrier execution, e.g. from batch.