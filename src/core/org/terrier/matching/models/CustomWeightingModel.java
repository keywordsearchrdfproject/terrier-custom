package org.terrier.matching.models;

import org.terrier.structures.postings.Posting;

public abstract class CustomWeightingModel extends WeightingModel {

	public CustomWeightingModel() {
		super();
	}
	
	public CustomWeightingModel( WeightingModel mod) {
		super();
		
		this.averageDocumentLength = mod.averageDocumentLength;
		this.c = mod.c;
		this.cs = mod.cs;
		this.documentFrequency = mod.documentFrequency;
		this.es = mod.es;
		this.i = mod.i;
		this.keyFrequency = mod.keyFrequency;
		this.numberOfDocuments = mod.numberOfDocuments;
		this.numberOfPointers = mod.numberOfPointers;
		this.numberOfTokens = mod.numberOfTokens;
		this.numberOfUniqueTerms = mod.numberOfUniqueTerms;
		this.rq = mod.rq;
		this.termFrequency = mod.termFrequency;
	}
	/**
	 * Returns score
	 * @param i index of the query word
	 * @param qi query word
	 * */
	public double customScore(Posting p, int i, String qi) {
		return this.score(p.getFrequency(), p.getDocumentLength());
	}
}
