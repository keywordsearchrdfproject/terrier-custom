package org.terrier.matching.models;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.StringTokenizer;

import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.RDFParser;
import org.openrdf.rio.Rio;
import org.openrdf.rio.helpers.StatementCollector;
import org.terrier.indexing.Document;
import org.terrier.indexing.FileDocument;
import org.terrier.indexing.tokenisation.Tokeniser;
import org.terrier.querying.Request;
import org.terrier.realtime.memory.MemoryIndex;
import org.terrier.structures.Index;
import org.terrier.structures.IndexOnDisk;
import org.terrier.structures.Lexicon;
import org.terrier.structures.LexiconEntry;
import org.terrier.structures.postings.Posting;

import it.unipd.dei.ims.terrier.utilities.BlazegraphUsefulMethods;
import it.unipd.dei.ims.terrier.utilities.PropertiesUsefulMethods;
import it.unipd.dei.ims.terrier.utilities.TerrierUsefulMethods;
import it.unipd.dei.ims.terrier.utilities.UrlUtilities;
import it.unipd.dei.ims.terrier.utilities.UsefulConstants;

/** Blanco - Elbassuoni Language Model implementation.
 * <p>
 * Second version. Including the use of Blazegraph and corrections in the use of properties.
 * 
 * <p>
 * property files where used properties are saved: properties/blanco.properties
 *<p>
 *list of properties we use:
 *<ul>
 *<li> directory where the answer subgraphs to rank are stored: blanco.subgraphs.directory
 *<li> directory where the rj indexes are stored: blanco.rj.indexes.directory
 *<li> directory of the index of all the R_j (all the collection of R_j): blanco.rj.index.directory
 *</ul>
 * 
 *
 * @author Dennis Dosso*/
public class BE_LM extends WeightingModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**Necessary variable to know when to do the first computations*/
	private boolean firstTime = false;
	
	//some useful static constants
	private long colCardinality;
	
	/** This map is used to save the values relative to the sums
	 * sum_k P(q_i | R_k) * P(R_k)
	 * */
	private Map<String, Double> sumRjMap;
	
	private double beta;

	private HashMap<String, String> rjMap;
	
	/** Map with the indexes to the single R_j files*/
	//XXX
	private Map<String, Index> RJIndexesMap;
	
	/** Index of the whole R_j collection*/
	private Index RjCollectionIndex;
	
	/** In this map we insert useful informations such as path and other data*/
	private Map<String, String> propertyMap;

	/** Counts the number of different R_j documents are derived
	 * from the collection. 
	 * */
	private int predicateCounter;

	/** The uniform probability given to the R_j.
	 * */
	private double uniformProbability;

	/** Average length of a Rj document*/
	private double averageRJLength;
	
	/** id to understand when we have changed document and it is necessary to do some cleaning*/
	private static int docIdentificator = -1;
	
	MemoryIndex rIndex;


	public BE_LM() {
		super();
		firstTime = true;
		colCardinality = 0L;
		rjMap = new HashMap<String, String> ();
		
		RJIndexesMap = new HashMap<String, Index>();
		
		predicateCounter = 0;
		averageRJLength = 0;
		
		sumRjMap = new HashMap<String, Double>();
		
		predicateCounter = 0;
		uniformProbability = 0;
		averageRJLength = 0;
		rIndex = new MemoryIndex();
		beta = 0.9;//putting beta to 0.9 as described in Yosi's PhD thesis

		try {
			propertyMap = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/main.properties");
			
			// this if is introduced to deal with the case of serial execution of multiple queries
			boolean serial = Boolean.parseBoolean(propertyMap.getOrDefault("multiple.queries", "true"));
			if(serial) {
				propertyMap.put("blanco.rj.index.directory", System.getProperty("blanco.rj.index.directory"));
				propertyMap.put("blanco.rj.indexes.directory", System.getProperty("blanco.rj.indexes.directory"));
				propertyMap.put("blanco.subgraphs.directory", System.getProperty("blanco.subgraphs.directory"));
				propertyMap.put("blanco.r.j.output.directory", System.getProperty("blanco.r.j.output.directory"));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//parameter beta, should be equal to the average document length in the whole collection
		c = 2500;
	}
	
	@Override
	public void finalize() {
//		System.out.println("closing all connections, folks");
		this.closeAllIndexes();
	}

	@Override
	public String getInfo() {
		return null;
	}

	@Override
	public double score(double tf, double docLength) {
		return 0;
	}
	
	/** Method re-implemented for the BE_LM.
	 * 
	 * @param p posting list
	 * @param i Identificator of the graph/document G where we are working inside Terrier. 
	 * @param qi the keyword that we are scoring
	 * */
	@Override
	public double score(Posting p, int i, String qi) {
		

		if(firstTime) {
			initialComputations();
			firstTime = false;
		}
		
		
		//main big directory where the graphs are stored
		String subgraphDirectoryPath = propertyMap.get("blanco.subgraphs.directory");
		//boolean to decide if we are using subdirectories or not in the answer subgraphs directory
		boolean fold = Boolean.parseBoolean(propertyMap.get("blanco.folding"));
		//need to understand the subdirectory

		//get the index
		Request r = this.rq;
		
		Index index = r.getIndex();

		//XXX debug
//		if(i%500==0 ) {
//			System.out.println("elaborated " + i + " graphs out of " + index.getCollectionStatistics().getNumberOfDocuments() +
//					" query word: " + qi
//					);
//		}

		try {
			//get the docno of the document
			String docno = index.getMetaIndex().getItem("docno", i);
			int docId = Integer.parseInt(docno);
			//get the path of the graph. We can use an hash function
			int offset = (int)(Math.floor((double) docId/2048) + 1);
			String subgraphPath;
			if(fold)
				subgraphPath = subgraphDirectoryPath + "/" + offset + "/" + docId + ".ttl";
			else
				subgraphPath =  subgraphDirectoryPath + "/" + docId + ".ttl";

			//get the model
			Collection<Statement> modelCollection = BlazegraphUsefulMethods.readOneGraph(subgraphPath);

			double theScore = scoreWithGraphAndQueryWord(modelCollection, qi);
			return theScore;

		} catch (RDFParseException | RDFHandlerException | IOException e) {
			e.printStackTrace();
		} catch(NumberFormatException e) {
			System.err.println("doc with internal Terrier id: " + i + " has no docno");
		}
		return 0;
	}
	
	private void closeAllIndexes() {
			try {
				if(this.RjCollectionIndex != null)
					this.RjCollectionIndex.close();
				
				for(Entry<String, Index> indexEntry : RJIndexesMap.entrySet()) {
					Index index = indexEntry.getValue();
					if(index != null)
						index.close();
				}
				this.RJIndexesMap.clear();
				
			} catch (IOException e) {
				e.printStackTrace();
			} 
	}
	
	private void initialComputations() {
		BE_LM_StaticVariablesHolder.setup(this.cs.getNumberOfTokens(), propertyMap);
		this.colCardinality = BE_LM_StaticVariablesHolder.getColCardinality();
		this.RJIndexesMap = BE_LM_StaticVariablesHolder.getRJIndexesMap();
		this.predicateCounter = BE_LM_StaticVariablesHolder.getPredicateCounter();
		this.RjCollectionIndex = BE_LM_StaticVariablesHolder.getRjCollectionIndex();
		this.uniformProbability = BE_LM_StaticVariablesHolder.getUniformProbability();
		this.averageRJLength = BE_LM_StaticVariablesHolder.getAverageRJLength();
		
	}
	
	private void initialComputationsWithIndexes() {
		System.out.println("Setting up for the Blanco Algorithm (file R_j) please wait...");
		//cardinality of the knowledge base (Col) composed by all the subgraphs retrieved
		colCardinality = this.cs.getNumberOfTokens();
		
		//SETUP
		//directory where all the R_j indexes are stored
		String rjIndexesDir = propertyMap.get("blanco.rj.indexes.directory");
		File rjIndxesFile = new File(rjIndexesDir);
		File[] rjIndexes = rjIndxesFile.listFiles();
		for(File file : rjIndexes) {
			//each file in this directory contains an index
			if(file.getName().equals(".DS_Store")) 
				continue;
			
			//open the index if necessary
			Index i = RJIndexesMap.get(file.getName());
			if(i == null) {
				Index index = IndexOnDisk.createIndex(file.getAbsolutePath(), "data");
				//save into the map
				RJIndexesMap.put(file.getName(), index);
			}
			//update the number of R_j files we have
			predicateCounter++;				
		}
		
		if(RjCollectionIndex == null) {
			String rjIndexDir = propertyMap.get("blanco.rj.index.directory");
			//open the index of the whole R_j collection and set it
			RjCollectionIndex = IndexOnDisk.createIndex(rjIndexDir, "data");
			//now the useful statistics:
			uniformProbability = (double) 1 / predicateCounter;
			averageRJLength = RjCollectionIndex.getCollectionStatistics().getAverageDocumentLength();
			System.out.println("initial computations completed");
		}
	}

	/**
	 * Computes the score for the "document" represented by the Model/Graph m 
	 * given the query word q_i following the computations in Elbassuoni et al.
	 * 
	 * i.e. computes p(q_i | G) = 1/n * sum_j[ p(q_i | D_j, r_j) ]
	 * <p>
	 * j iterates over the triples of G.
	 * */
	private double scoreWithGraphAndQueryWord(Collection<Statement> model, String q) {
		double score = 0;
		int tripleCounter = 0;
		Iterator<Statement> iter =  model.iterator();
		double mu = 0;
		
		iter = model.iterator();
		//iteration over the triples of the graph (sum over j)
		while(iter.hasNext()) {
			Statement t = iter.next();
			tripleCounter++;//building the value n

			/*we compute P(q_i | t_j) = P(q_i | D_j, r_j)
			 * using Bayes:
			 * P(q_i | D_j, r_j) = P(q_i | D_j)*P(r_j|q_i, D_j) / P(r_j | D_j)
			 * = beta * P(q_i|D_j) * P(r_j | q_i) / P(r_j | D_j) + (1 - beta) * P(q_i | D_j)
			 * So we need 2 values.
			 * 
			 * That's beacaus P(r_j | D_j) = 1, given that r_j will surely appear in the document
			 * D_j corresponding to the triple t_j where r_j belongs.*/

			//P(q_i | D_j) ( != P(q_i | t_j) ) 
			double pQGivenDocument = computePQGivenDocument(q, t, mu);
			
			//probability P(r_j | q_i)
			double pRjGivenQ = computePRjGivenQi(q, t);
			
			//compute P(q_i | t_j ) = P(q_i | D_j, r_j) = beta P(q_i | D_j) P(r_j | q_i) + (1-beta) P(q_i | D_j)
			double increment = (double)(beta * pQGivenDocument * pRjGivenQ + (1 - beta) * pQGivenDocument);
			score += increment;
		}
		
		//the final score id obtained dividing by n, the number of triples in G.
		score = (double) score / tripleCounter;
		//transform the score in its log, so it can be added
		return WeightingModelLibrary.log(score);
	}
	
	/** Computes the value P(r_j | q_i) following the Bayes' formula:
	 * 
	 * P(r_j | q_i) = P(q_i | R_j) * P(R_j) / sum( P(q_i| R_k) * P(R_k))
	 * */
	private double computePRjGivenQi(String qi, Statement t) {

		//P(q_i | R_j)
		double pQiGivenRj = computePQiGivenRj(qi, t);

		//sum( P(q_i| R_k) * P(R_k))
		double denominator = computeSumPQiRkPRk(qi);

		//P(q_i | R_j) * P(R_j)
		double numerator = pQiGivenRj * uniformProbability;

		return (double) numerator / denominator;
	}
	
	/**
	 * Computes the value
	 * sum_k P(q_i | R_k) * P(R_k)
	 * */
	private double computeSumPQiRkPRk(String qi) {
		
		Double i = this.sumRjMap.get(qi);
		if(i!=null)
			return i;
		
		double sum = 0;
		//varying on the k
		for(Entry<String, Index> entry : RJIndexesMap.entrySet()) {
			String rj = entry.getKey();
			//  P(q_i | R_k) * P(R_k)
			sum = sum + computePQiGivenRj(qi, rj) * uniformProbability;
		}
		
//		for(String rj : this.rjList) {
//			rj = UrlUtilities.takeFinalWordFromIRI(rj);
//			sum = sum + computePQiGivenRj(qi, rj) * uniformProbability;
//		}
		
		sumRjMap.put(qi, sum);
		
		return (double) sum;
	}
	
	/** Computes the value
	 * 
	 * P(q_i|R_j) = alpha c(q_i, R_j)/|R_j| + (1-alpha) * c(q_i, ColR) / |ColR|
	 * 
	 * @param rj the predicate defining Rj (key of thr Indexes map, in the version used as name of the directories
	 * where the indexes are stored)
	 * */
	private double computePQiGivenRj(String qi, String rj) {
		//compute alpha
//		this.rIndex.getCollectionStatistics().getAverageDocumentLength();
		Index rjIndex = RJIndexesMap.get(rj);
		//|R_j|
		long docLen = rjIndex.getCollectionStatistics().getNumberOfTokens();
		
		double alpha = (double) (docLen / (double)(docLen + this.averageRJLength));

		Lexicon<String> lex = rjIndex.getLexicon();
		LexiconEntry le = lex.getLexiconEntry(qi);

		//c(q_i, R_j)
		long c = 0;
		if(le!=null)
			c = le.getFrequency();
		//c(q_i, R_j) / |R_j|
		double firstTerm = (double) c / docLen;
		
		//|CollR|
		long collLen = RjCollectionIndex.getCollectionStatistics().getNumberOfTokens();
		
		lex = RjCollectionIndex.getLexicon();
		le = lex.getLexiconEntry(qi);
		if(le!=null)
			c = le.getFrequency();
		else
			c=0;
		
		double secondTerm = (double) c / collLen;
		
//		double secondTerm = computeLikelihoodQiInAllR(qi);

		return (alpha * firstTerm + (1 - alpha) * secondTerm);

	}
	
	/** Computes the value
	 * 
	 * P(q_i|R_j) = alpha c(q_i, R_j)/|R_j| + (1-alpha) * c(q_i, ColR) / |ColR|
	 * */
	private double computePQiGivenRj(String qi, Statement t) {
		
		//get the index about the Rj file corresponding to the predicate of t
		String rj = t.getPredicate().toString();
		String rjKey = UrlUtilities.takeFinalWordFromIRI(rj);
		Index jIndx = RJIndexesMap.get(rjKey);
		
		//|R_j|
		long docLen = jIndx.getCollectionStatistics().getNumberOfTokens();
		
		//alpha = |R_j| / (|R_j| + mu), where mu is the average length of the collection of R_j files
		double alpha = (double) (docLen / (double)(docLen + this.averageRJLength));
		
		Lexicon<String> lex = jIndx.getLexicon();
		
		LexiconEntry le = lex.getLexiconEntry(qi);

		//c(q_i, R_j)
		int c = 0;
		if(le!=null)
			c = le.getFrequency();
		//c(q_i, R_j)/|R_j|
		double firstTerm = (double) c / docLen;
		
		//c(q_i, ColR)
		Lexicon<String> lexR = RjCollectionIndex.getLexicon();
		LexiconEntry leR = lexR.getLexiconEntry(qi);
		c = 0;
		if(leR != null)
			c = leR.getFrequency();
		
		//|ColR|
		long colLength = RjCollectionIndex.getCollectionStatistics().getNumberOfTokens();
		//c(q_i, ColR) / |ColR|
		double secondTerm = (double) c / colLength;

		return (double) (alpha * firstTerm + (1 - alpha) * secondTerm);

	}
	
	/**Computes P(q_i | D_j) = = alpha * c(q_i, D_j)/|D_j| + (1-alpha) c(q_i, Col) / |Col|
	 * where alpha = |D_j|/(|D_j| + mu), following the Dirichlet smoothing principle
	 * 
	 * @param mu the average length of the graph (useless for now)
	 * */
	private double computePQGivenDocument(String qi, Statement t, double mu) {
		//get the words from the triple
		String tripleDocument = BlazegraphUsefulMethods.fromStatementToDocument(t);
		//index the words
		MemoryIndex tripleIndex;

		//|D_j|
		long docLen = 0;
		//c(q_i, D_j)
		int tf = 0;

		double alpha = 0;
		try {
			//index the triple document (index of D_j) From here we get all the data we need
			tripleIndex = TerrierUsefulMethods.getMemoryIndexFromDocument(tripleDocument);

			//lexicon of the document
			Lexicon<String> lex = tripleIndex.getLexicon();

			//c(q_i, D_j)
			LexiconEntry le = lex.getLexiconEntry(qi);
			if(le != null)
				tf = le.getFrequency();

			//|D_j|
			docLen = tripleIndex.getCollectionStatistics().getNumberOfTokens();


			//alpha =|D_j| / ( |D_j| + mu ), where mu is the average document length of the triples in the Collection
			alpha = (double) (docLen / (docLen + super.averageDocumentLength));

			//close the index
			tripleIndex.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		//c(q_i, D_j) / |D_j|
		double firstTerm = (double)tf / docLen;

		//c(q_i, Col) / |Col|
		double secondTerm = super.termFrequency / numberOfTokens;

		//and compute the median with alpha
		return (alpha * firstTerm) + ((1 - alpha) * secondTerm);
	}

	/**Returns the term frequency of the word q in the document represented 
	 * by the triple t
	 * 
	 * */
	private static int getFrequencyInTriple(String qi, Statement t) {
		//get the words from the triple
		String tripleDocument = BlazegraphUsefulMethods.fromStatementToDocument(t);
		//index phase on these words
		tripleDocument = TerrierUsefulMethods.getDocumentWordsWithTerrierAsString(tripleDocument);
		//frequency
		try {
			return TerrierUsefulMethods.getFrequencyOfWordInTextViaTerrier(tripleDocument, qi);
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	private int countRJCollectionSize() {
		int answer = 0;
		for(Entry<String, String> entry : rjMap.entrySet()) {
			String doc = entry.getValue();
			StringTokenizer st = new StringTokenizer(doc);
			answer += st.countTokens();
		}
		return answer;
	}

	/** Writes all the Rj in one TREC file. */
	private void writeTheRjFiles() {
		Map<String, String> map;
		try {
			map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/blanco.properties");
			String outputDirectory = map.get("blanco.r.j.output.directory");

			Path outputPath = Paths.get(outputDirectory + "/rj.trec");

			BufferedWriter writer = Files.newBufferedWriter(outputPath, UsefulConstants.CHARSET_ENCODING);
			int docno = 0;
			for(Entry<String, String> entry : rjMap.entrySet()) {
				String predicate = entry.getKey();
				String doc = entry.getValue();
				writer.write("<DOC>");
				writer.newLine();

				writer.write("<PREDICATE>" + predicate + "</PREDICATE>");
				writer.newLine();

				writer.write("<DOCNO>" + (docno++) + "</DOCNO>");
				writer.newLine();

				writer.write(doc);
				writer.newLine();

				writer.write("</DOC>");
				writer.newLine();

				writer.flush();
			}

			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		rjMap.clear();
	}

	/** Given the path of a RDF file, use it to increment the R_j documents.
	 * */
	private void addOneFileToRj(File graphFile) {
		try {
			//read the file and incorporate in a model
			InputStream inputStream = new FileInputStream(graphFile);
			//prepare a collector to contain the triples
			StatementCollector collector = new StatementCollector();
			//read the file
			RDFParser rdfParser = Rio.createParser(RDFFormat.TURTLE);
			//link the collector to the parser
			rdfParser.setRDFHandler(collector);
			//parse the file
			rdfParser.parse(inputStream, "");
			//now get the triples/statements composing the graph
			Collection<org.openrdf.model.Statement> statements = collector.getStatements();
			Iterator<org.openrdf.model.Statement> iter = statements.iterator();

			while(iter.hasNext()) {
				//get the predicate r of the triple
				Statement t = iter.next();
				URI predicate = t.getPredicate();
				if(! rjMap.containsKey(predicate.toString()) ) {//first time we see this r
					//obtain the document connected to the statement
					String tripleDoc = BlazegraphUsefulMethods.fromStatementToDocument(t);
					//index it and obtained the version read throug Terrier
					String rDoc = TerrierUsefulMethods.getDocumentWordsWithTerrierAsString(tripleDoc);
					rjMap.put(predicate.toString(), rDoc);
					this.predicateCounter++;
				} else { //predicate already present
					//get document of this triple
					String tripleDoc = BlazegraphUsefulMethods.fromStatementToDocument(t);
					//index it and obtained the version read throug Terrier
					String tDocTer = TerrierUsefulMethods.getDocumentWordsWithTerrierAsString(tripleDoc);
					String rDoc = rjMap.get(predicate.toString());
					//add the words to the document already created
					rDoc = rDoc + " " + tDocTer;
					rjMap.put(predicate.toString(), rDoc);
				}
			}

		} catch (RDFParseException | RDFHandlerException | IOException e) {
			e.printStackTrace();
		}

	}

}
