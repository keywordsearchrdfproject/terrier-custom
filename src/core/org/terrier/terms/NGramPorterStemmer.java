package org.terrier.terms;

/** A little extension of the Porter Stemmer in order to deal with N-Grams.
 * 
 * @author Dennis Dosso
 * */
public class NGramPorterStemmer extends PorterStemmer {

	public NGramPorterStemmer() {
		super();
	}
	
	/** 
	 * Constructs an instance of PorterStemmer.
	 * @param next
	 */
	public NGramPorterStemmer(TermPipeline next)
	{
	   super(next);
	}
	
	/** Re-implementation of the method, in order to deal with the
	 * N-grams.
	 * */
	@Override
	public void processTerm(String t) {
		//divide the N-gram
		String[] parts = t.split(" ");
		//stem each word of the N-gram separatedly
		for(int i = 0; i < parts.length; ++i) {
			if(parts[i]==null)
				return;
			parts[i] = stem(parts[i]);
		}
		//re-build the string with the stemmed words
		String whole = "";
		for(String s : parts) {
			whole = whole + " " + s;
		}
		whole = whole.trim();
		//pass the string to the next element of the pipeline
		next.processTerm(whole);
	}
}
