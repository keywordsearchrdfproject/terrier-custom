package org.terrier.terms;

public class NGramStopwords extends Stopwords {

	public NGramStopwords(TermPipeline _next) {
		super(_next);
	}

	public NGramStopwords(TermPipeline _next, String StopwordsFile) {
		super(_next, StopwordsFile);
	}
	
	public NGramStopwords(TermPipeline _next, String[] StopwordsFiles) {
		super(_next, StopwordsFiles);
	}
	
	/** The string t is supposed to be a N-gram, that
	 * is, N words sparated by a space. The method checks 
	 * the words separately and them passes them
	 * to the next element of the pipeline.
	 * */
	@Override
	public void processTerm(final String t) {
		if(t==null)
			return;
		String[] parts = t.split(" ");
		for(String s : parts) {
			if(stopWords.contains(s))
				return;
		}
		next.processTerm(t);
	}
	
}
