package org.terrier.terms;

/** This class simply extends a little bit the Stopwords one
 * considering bigrams.
 * */
public class BigramStopwords extends Stopwords {

	public BigramStopwords(TermPipeline _next) {
		super(_next);
	}

	public BigramStopwords(TermPipeline _next, String StopwordsFile) {
		super(_next, StopwordsFile);
	}
	
	public BigramStopwords(TermPipeline _next, String[] StopwordsFiles) {
		super(_next, StopwordsFiles);
	}

	/** The string t is supposed to be a bigram, that
	 * is, two words sparated by a space. The method checks 
	 * the two words separately and them passes them
	 * to the next element of the pipeline.
	 * */
	@Override
	public void processTerm(final String t) {
		String[] parts = t.split(" ");
		if(stopWords.contains(parts[0]) || stopWords.contains(parts[1]))
			return;
		next.processTerm(t);
	}


}
