package org.terrier.indexing.tokenisation;

import java.io.IOException;
import java.io.Reader;

import org.terrier.utility.ApplicationSetup;

/** This class implements a tokeniser that divides the words inside a 
 * document in non-overlapping trigrams. For example, the document string:
 * <p>
 * To be or not to be
 * </p>
 * Will be converted in the trigrams:
 * <p>
 * [To be or], [not to be]
 * 
 * @author Dennis Dosso
 * */
public class SeparatedTrigramStemmer extends Tokeniser {

	/**Max length for a string inside a bigram (so a bigram can be twice as long)*/
	static final int MAX_TERM_LENGTH = ApplicationSetup.MAX_TERM_LENGTH;

	/** Set to true if you want to discard tokens that are too long*/
	protected final static boolean DROP_LONG_TOKENS = true;
	
	@Override
	public TokenStream tokenise(Reader reader) {
		return new SeparatedTrigramTokenStream(reader);
	}
	
	//XXX no use of the check method in this class
	
	static class SeparatedTrigramTokenStream extends TokenStream {
		
		/**Character that is going to be read from the Reader*/
		int ch;
		/** A static element to keep track of what was the last charread*/
		static int lastCh;

		/** Boolean value representing if we have reached the end of the string. */
		boolean eos = false;

		String first, second, third;
		Reader reader;
		
		static int counter = 0;
		
		/**Builder. Checks that the reader contains a string.
		 * */
		public SeparatedTrigramTokenStream(Reader _br) {
			this.reader = _br;
			if(this.reader == null) {
				this.eos = true;
			}
			first = null;
			second = null;
			third = null;
		}
		
		/** Returns true if we have reached the end of the string. NB: it is not 
		 * necessarily true that when the returned string is null we have reached the end of the
		 * string.
		 * */
		@Override
		public boolean hasNext() {
			return ! eos;
		}
		
		/** returns the next trigram token
		 * */
		@Override
		public String next() {
			try {
				//read the first character
				ch = this.reader.read();
				while(ch != -1) {
					//skip non-alphanumeric characters, we go over all the char that we don't like
					while (ch != -1 && (ch < 'A' || ch > 'Z') && (ch < 'a' || ch > 'z')
							&& (ch < '0' || ch > '9') 
							/* removed by Craig: && ch != '<' && ch != '&' */
							) 
					{
						//skip
						ch = reader.read();
						counter++;
					}
					//if we are here, we have skipped the characters that we don't like


					//create a string builder to build our string
					StringBuilder sw = new StringBuilder(MAX_TERM_LENGTH);

					//now accept all alphanumeric charaters
					while (ch != -1 && (
							((ch >= 'A') && (ch <= 'Z'))
							|| ((ch >= 'a') && (ch <= 'z'))
							|| ((ch >= '0') && (ch <= '9')) ))
					{

						/* add character to word so far */
						//add the last read character
						sw.append((char)ch);
						//update the char
						ch = reader.read();
						counter++;
					}
					
					//if we are here, we have encountered a bad char, so we break the document.
					//In sw there is a token to read
					if(first==null) {
						//we have just created the first string
						first = sw.toString();
					} else if (first!= null && second == null) {
						//now give me the third string
						second = sw.toString();
					} else if (first != null && second != null && third == null) {
						third = sw.toString();
						String returningString = first + " " + second + " " + third;
						//reset everything
						first = second = third = null;
						return returningString;
					}
					
				}
				//end of the file. Return one last null value, then nothing more
				eos = true;
				return null;
			} catch (IOException e) {
				e.printStackTrace();
			}


			return null;
		}
		
		
		
	}

}
