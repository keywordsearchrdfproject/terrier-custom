package org.terrier.indexing.tokenisation;

import java.io.IOException;
import java.io.Reader;
import java.nio.CharBuffer;

import org.terrier.utility.ApplicationSetup;

public class BigramTokeniser extends Tokeniser {

	/**Max length for a string inside a bigram (so a bigram can be twice as long)*/
	static final int MAX_TERM_LENGTH = ApplicationSetup.MAX_TERM_LENGTH;

	/** Set to true if you want to discard tokens that are too long*/
	protected final static boolean DROP_LONG_TOKENS = true;

	/** Returns a TokenStream which implements the way this Tokeniser reads the
	 * string provided in the reader*/
	@Override
	public TokenStream tokenise(final Reader reader) {
		return new BigramTokenStream(reader);
	}



	/** Utilize the {@link EnglishTokeniser} method to check the string.
	 * */
	static String check(String s) {
		return EnglishTokeniser.check(s);
	}

	static class BigramTokenStream extends TokenStream {

		/**Character that is going to be read from the Reader*/
		int ch;
		/** A static element to keep track of what was the last charread*/
		static int lastCh;

		/** Boolean value representing if we have reached the end of the string. */
		boolean eos = false;

		String first, second;
		Reader reader;

		int counter = 0;

		/**Builder. Checks that the reader contains a string.
		 * */
		public BigramTokenStream(Reader _br) {
			this.reader = _br;
			if(this.reader == null) {
				this.eos = true;
			}
			first = null;
			second = null;
		}


		/** Returns true if we have reached the end of the string. NB: it is not 
		 * necessarily true that when the returned string is null we have reached the end of the
		 * string.
		 * */
		@Override
		public boolean hasNext() {
			return ! eos;
		}

		@Override
		public String next() {
			try {
				//read the first character
				ch = this.reader.read();
				while(ch != -1) {
					//skip non-alphanumeric characters, we go over all the char that we don't like
					while (ch != -1 && (ch < 'A' || ch > 'Z') && (ch < 'a' || ch > 'z')
							&& (ch < '0' || ch > '9') 
							/* removed by Craig: && ch != '<' && ch != '&' */
							) 
					{
						//skip
						ch = reader.read();
						counter++;
					}
					//if we are here, we have skipped the characters that we don't like


					//create a string builder to build our string
					StringBuilder sw = new StringBuilder(MAX_TERM_LENGTH);

					//now accept all alphanumeric charaters
					while (ch != -1 && (
							((ch >= 'A') && (ch <= 'Z'))
							|| ((ch >= 'a') && (ch <= 'z'))
							|| ((ch >= '0') && (ch <= '9')) ))
					{

						/* add character to word so far */
						//add the last read character
						sw.append((char)ch);
						//update the char
						ch = reader.read();
						counter++;
					}

					if(first==null) {
						//we have just created the first string
						first = sw.toString();
					} else if (first!= null && second == null) {
						second = sw.toString();
						String one = first;
						String two = second;
						first = second;
						second = null;
						if(one.length() > MAX_TERM_LENGTH || two.length() > MAX_TERM_LENGTH) {
							if(DROP_LONG_TOKENS)
								return null;
						}

						//check the nature of the strings (in order to avoid noise in the tokens)
						one = check(one);
						two = check(two);
						if(one.length()>0 && two.length()>0)
							//in case both words are good, keep them and return the bigram
							return one + " " + two;
						if(one.length()>0 && two.length()==0) {
							//in case the new word in noise, discard it returning the string first to 
							//the previous value. In this way, we are 'jumping' over
							//the noise token
							first = one;
						}
					}
				}
				//end of the file. Return one last null value, then nothing more
				eos = true;
				return null;
			} catch (IOException e) {
				e.printStackTrace();
			}


			return null;
		}

	}

}
