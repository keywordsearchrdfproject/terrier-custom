package it.unipd.dei.ims.terrier.bigram;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.terrier.indexing.Document;
import org.terrier.indexing.FileDocument;
import org.terrier.indexing.tokenisation.BigramTokeniser;
import org.terrier.indexing.tokenisation.Tokeniser;
import org.terrier.terms.PorterStemmer;
import org.terrier.terms.Stopwords;

public class BigramUsefulMethods {

	private String document;

	public BigramUsefulMethods() {

	}

	public BigramUsefulMethods(String d) {
		document = d;
	}

	/** Extrapolates the bigram from a document string. The returned list doesn't contains 
	 * duplicates (useful to obtain for example a list of bigrams on which iterate).
	 * <p>
	 * NB: the parameter string must have been processed in the pipeline composed by the stop-word removal and the stemmer.
	 * For example:
	 * <xmp>
	 * Stopwords stop = new Stopwords(null);
	   PorterStemmer stemmer = new PorterStemmer();
	 * doc = new FileDocument(new StringReader("you text here"), new HashMap(), Tokeniser.getTokeniser());
			while(!doc.endOfDocument()) {//for every word in the document
				String s = doc.getNextTerm();
				if(s==null)
					break;
				//check that it is not a stopword
				if(!stop.isStopword(s)) {
					//stem the word
					s = stemmer.stem(s);
					//your code here
					//...
				}
			}
	 * </xmp>
	 * 
	 * */
	public static List<Bigram> extractDistinctBigramsFromString(String elaborandum) {
		List<Bigram> bigramList = new ArrayList<Bigram>();
		Document doc = new FileDocument(new StringReader(elaborandum), new HashMap(), new BigramTokeniser());

		while(!doc.endOfDocument()) {
			String bigramString = doc.getNextTerm();
			if(bigramString==null)
				continue;
			String[] parts = bigramString.split(" ");
			Bigram newBigram = new Bigram(parts[0], parts[1]);
			if(!checkIfListContainsBigram(bigramList, newBigram)){
				//if this is a new bigram
				bigramList.add(newBigram);
			}
		}
		
	
		return bigramList;
	}
	
	/** Estracts distinct bigrams from a list of strings aready pipelined.
	 * */
	public static List<Bigram> extractDistinctBigramsFromListOfString(List<String> stringList) {
		List<Bigram> bigramList = new ArrayList<Bigram>();
		String elaborandum = "";
		for(String s : stringList) {
			elaborandum = elaborandum + " " + s;
		}
		return extractDistinctBigramsFromString(elaborandum);
	}
	
	public static List<Bigram> extractDistinctBigramsFromListOfStringWithPipeline(List<String> stringList) {
		String elaborandum = "";
		for(String s : stringList) {
			elaborandum = elaborandum + " " + s;
		}
		return extractDistinctBigramsFromStringWithPipeline(elaborandum);
	}
	
	/** This methods operates like {@link extractDistinctBigramsFromString}, but also performs stopwords removal
	 * and stemming*/
	public static List<Bigram> extractDistinctBigramsFromStringWithPipeline(String elaborandum) {
		List<Bigram> bigramList = new ArrayList<Bigram>();
		Document doc = new FileDocument(new StringReader(elaborandum), new HashMap(), new BigramTokeniser());

		Stopwords stop = new Stopwords(null);
		PorterStemmer stemmer = new PorterStemmer();
		
		while(!doc.endOfDocument()) {
			String bigramString = doc.getNextTerm();
			if(bigramString==null)
				continue;
			String[] parts = bigramString.split(" ");
			if(stop.isStopword(parts[0]) || stop.isStopword(parts[1]))
				continue;
			parts[0] = stemmer.stem(parts[0]);
			parts[1] = stemmer.stem(parts[1]);
			Bigram newBigram = new Bigram(parts[0], parts[1]);
			if(!checkIfListContainsBigram(bigramList, newBigram)){
				//if this is a new bigram
				bigramList.add(newBigram);
			}
		}
		   
		return bigramList;
	}

	/** This method extrapolates all the bigrams from a string after indexing with Terrier. Duplictes are
	 * allowed. Useful when you want to compute the frequency of bigrams tokens. 
	 * <p>
	 * The returned list is a bigram token representation of the document*/
	public static List<Bigram> extractBigramsFromString(String elaborandum) {

		List<Bigram> bigramList = new ArrayList<Bigram>();
		Document doc = new FileDocument(new StringReader(elaborandum), new HashMap(), Tokeniser.getTokeniser());

		String first = doc.getNextTerm();
		while(!doc.endOfDocument()) {
			String second = doc.getNextTerm();
			if(second==null)
				break;
			Bigram newBigram = new Bigram(first, second);
			bigramList.add(newBigram);
			first = second;
		}

		return bigramList;
	}
	
	/** This method extrapolates all the bigrams from a string after indexing with Terrier. Duplictes are
	 * allowed. Useful when you want to compute the frequency of bigrams tokens. 
	 * <p>
	 * The returned list is a bigram token representation of the document*/
	public static List<Bigram> extractBigramsFromStringWithPipeline(String elaborandum) {

		List<Bigram> bigramList = new ArrayList<Bigram>();
		Document doc = new FileDocument(new StringReader(elaborandum), new HashMap(), new BigramTokeniser());

		//stopwords and stemmer
		Stopwords stop = new Stopwords(null);
		PorterStemmer stemmer = new PorterStemmer();
		
		while(!doc.endOfDocument()) {
			String bigram = doc.getNextTerm();
			if(bigram==null)
				continue;
			String[] parts = bigram.split(" ");
			if(stop.isStopword(parts[0]) || stop.isStopword(parts[1]))
				continue;
			parts[0] = stemmer.stem(parts[0]);
			parts[1] = stemmer.stem(parts[1]);
			Bigram newBigram = new Bigram(parts[0], parts[1]);
			bigramList.add(newBigram);
		}

		return bigramList;
	}

	/** Gven a bigram and a list of bigrams, representing your document, this method gives you the frequency
	 * of the bigram inside the dcument. 
	 * */
	public static int getBigramFrequency(Bigram bigram, List<Bigram> document) {
		int count = 0;
		//deal with possible null pointers
		if(bigram == null)
			return count;
		
		for(Bigram b : document) {
			//deal with possible null pointers
			if(b == null)
				continue;
			//for every bigram in the document, we check if our bigram matches
			if(b.equals(bigram)) {
				count++;
			}
		}
		return count;
	}




	/** Check if the provided Bigram is contained in the provided list of bigrams. 
	 * */
	public static boolean checkIfListContainsBigram (List<Bigram> list, Bigram b) {
		for(Bigram bigram : list) {
			if(bigram.equals(b))
				return true;
		}
		return false;
	}
}
