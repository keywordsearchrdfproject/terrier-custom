package it.unipd.dei.ims.terrier.bigram;

/**This class represents an <b>unordered bigram</b>, a pair of words close to each other.
 * */
public class Bigram {

	private String first, second;
	
	/** The frequency of the unordered bigram in the document. Default at 0*/
	private int frequency = 0;
	
	public Bigram() {
		
	}
	
	public Bigram(String f, String s) {
		first = f;
		second = s;
		frequency = 1;
	}
	
	/** Returns true if the two bigrams are equal.*/
	public boolean equals(Bigram b) {
		try {
			if(this.second.equals(b.second) && this.first.equals(b.first) )
				return true;
			else if(this.second.equals(b.first) && this.first.equals(b.second))
				return true;
			return false;
			
		} catch (NullPointerException e) {
			System.err.println("null pointer exception with bigram " + b);
		}
		return false;
	}

	public String getFirst() {
		return first;
	}

	public void setFirst(String first) {
		this.first = first;
	}

	public String getSecond() {
		return second;
	}

	public void setSecond(String second) {
		this.second = second;
	}

	public int getFrequency() {
		return frequency;
	}

	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}
	
	public String toString() {
		return "[" + this.first + ", " + this.second + "]";
	}
	
	/** permits to print the standard expression of the Bigram. 
	 * Here, this is intended as 
	 * <p>
	 * first string + " " + second string
	 * */
	public String getStandardExpression() {
		return this.first + " " + this.second;
	}
}
