package it.unipd.dei.ims.terrier.utilities;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.terrier.indexing.Document;
import org.terrier.indexing.FileDocument;
import org.terrier.indexing.tokenisation.BigramTokeniser;
import org.terrier.indexing.tokenisation.EnglishTokeniser;
import org.terrier.indexing.tokenisation.Tokeniser;
import org.terrier.realtime.memory.MemoryIndex;
import org.terrier.structures.Lexicon;
import org.terrier.structures.LexiconEntry;
import org.terrier.terms.PorterStemmer;
import org.terrier.terms.Stopwords;

/**This class contains useful methods based on Terrier.
 * 
 * */
public class TerrierUsefulMethods {

	/** Given a string representing a short test, this method indexes in local
	 * memory this document and return a list of words found by terrier inside of it.
	 * THis set of words will have passed the stemming and stop-word removal
	 * phases of Terrier if specified in the properties file. Alphabetical
	 * order will be imposed to the words.
	 * 
	 * */
	public static List<String> getDocumentWordsWithTerrierAsList(String text) {
		List<String> list = new ArrayList<String>();
		
		//creation of the document from the string of text
		Document document = new FileDocument(new StringReader(text), new HashMap(), Tokeniser.getTokeniser());
		//index in local memory
		MemoryIndex memIndex = new MemoryIndex();
		//index the document
		try {
			//index it
			memIndex.indexDocument(document);
			//get the lexicon
			Lexicon<String> lex = memIndex.getLexicon();
			//retrieve the words
			for(int i = 0; i < lex.numberOfEntries(); ++i) {
				String w = lex.getIthLexiconEntry(i).getKey();
				list.add(w);
			}
			memIndex.close();
		} catch (Exception e) {
			System.err.println("[ERROR] unable to extrapolate the words from the string " + text);
			e.printStackTrace();
		}
		
		return list;
	}
	
	
	
	/** This method operates the removal of stopwords from a document string and 
	 * the stemming and returns the result. It does not change alphabetically the order of 
	 * the words. Duplicates in the list are maintained
	 * */
	public static List<String> pipelineStopWordsAndStemmerToList(String document) {
		Document doc = new FileDocument(new StringReader(document), new HashMap(), Tokeniser.getTokeniser());

		List<String> list = new ArrayList<String>();
		
		Stopwords stop = new Stopwords(null);
		PorterStemmer stemmer = new PorterStemmer();
		String s;
		while(!doc.endOfDocument()) {
			s = doc.getNextTerm();
			if(s==null)
				break;
			if(!stop.isStopword(s)) {
				s = stemmer.stem(s);
				list.add(s);
			}
		}

		return list;
	}
	
	/** This method operates the removal of stopwords from a document string and 
	 * the stemming and returns the result. It does not change alphabetically the order of 
	 * the words. Duplicates are removed from the list
	 * */
	public static List<String> pipelineStopWordsAndStemmerToListDitinctWords(String document) {
		Document doc = new FileDocument(new StringReader(document), new HashMap(), Tokeniser.getTokeniser());

		List<String> list = new ArrayList<String>();
		
		Stopwords stop = new Stopwords(null);
		PorterStemmer stemmer = new PorterStemmer();
		String s;
		while(!doc.endOfDocument()) {
			s = doc.getNextTerm();
			if(s==null)
				break;
			if(!stop.isStopword(s)) {
				s = stemmer.stem(s);
				if(!list.contains(s))
					list.add(s);
			}
		}
		return list;
	}
	
	/** Given a string representing a short test, this method indexes in local
	 * memory this document and return a string of words obtained in this way.
	 * This set of words will have passed the stemming and stop-word removal
	 * phases of Terrier if specified in the properties file.
	 * 
	 * */
	public static String getDocumentWordsWithTerrierAsString(String text) {
		String r = "";
		//creation of the document from the string of text
		Document document = new FileDocument(new StringReader(text), new HashMap(), Tokeniser.getTokeniser());
		//index in local memory
		MemoryIndex memIndex = new MemoryIndex();
		//index the document
		try {
			//index it
			memIndex.indexDocument(document);
			//get the lexicon
			Lexicon<String> lex = memIndex.getLexicon();
			//retrieve the words
			for(int i = 0; i < lex.numberOfEntries(); ++i) {
				String w = lex.getIthLexiconEntry(i).getKey();
				r = r + " " + w;
			}
			memIndex.close();
		} catch (Exception e) {
			System.err.println("[ERROR] unable to extrapolate the words from the string " + text);
			e.printStackTrace();
		}
		
		return r;
	}
	
	/** Creates an index in primary memory given the string. 
	 * Remember to close the index.
	 *  */
	public static MemoryIndex getMemoryIndexFromDocument(String text) throws Exception {
		Document document = new FileDocument(new StringReader(text), new HashMap(), Tokeniser.getTokeniser());
		MemoryIndex memIndex = new MemoryIndex();
		//index the document
		memIndex.indexDocument(document);
		
		return memIndex;
	}
	
	/** Creates an index in primary memory given the string. 
	 * Remember to close the index.
	 *  */
	public static MemoryIndex getBigramMemoryIndexFromDocument(String text) throws Exception {
		Document document = new FileDocument(new StringReader(text), new HashMap(), new BigramTokeniser());
		MemoryIndex memIndex = new MemoryIndex();
		//index the document
		memIndex.indexDocument(document);
		
		return memIndex;
	}
	
	public static int getFrequencyOfWordInTextViaTerrier(String text, String word) throws Exception {
		Document document = new FileDocument(new StringReader(text), new HashMap(), Tokeniser.getTokeniser());
		MemoryIndex memIndex = new MemoryIndex();
		//index the document
		memIndex.indexDocument(document);
		Lexicon<String> lex = memIndex.getLexicon();
		LexiconEntry le = lex.getLexiconEntry(word);
		memIndex.close();
		if(le != null) {
			return le.getFrequency();
		}
		else {
			return 0;
		}
		
	}
	
}
