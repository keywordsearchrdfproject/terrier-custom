package it.unipd.dei.ims.terrier.utilities;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/** This class contains useful constants (incredible, isn't it?)
 * 
 * */
public class UsefulConstants {

	public final static Charset CHARSET_ENCODING = StandardCharsets.UTF_8;
	
	public final static String NTRIPLE = "N-TRIPLE";
	
	public final static String TURTLE = "TURTLE";
}
